﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacadeExample.Forms;
using FacadeExample.Models;
using FacadeExample.Services;

namespace FacadeExample.Facades
{
    class UserFacade
    {
        //just empty comment
        private readonly ValidationService validationService;
        private readonly IDataService dataService;
        private readonly SignInService signInService;

        public UserFacade()
        {
            this.dataService = new XmlDataService();


            validationService = new ValidationService(dataService);
            signInService = new SignInService();
        }

        public void Register(string login, string password, string confirmPassword, RegisterForm form)
        {
            bool modelState = validationService.ValidateRegisterForm(login, password, confirmPassword, form);
            if (modelState)
            {
                User user = new User { Login = login, Password = password };
                dataService.AddUser(user);
                signInService.SignIn(user, form);
            }
        }

        public void SignIn(string login, string password, LoginForm form)
        {
            bool modelState = validationService.ValidateLoginForm(login, password, form);
            if (modelState)
            {
                User user = dataService.FindUser(u => u.Login == login);
                signInService.SignIn(user, form);
            }
        }

        public void AddOrder(string orderName, string userName, OrderForm form)
        {
            bool modelState = validationService.ValidateOrderForm(orderName, form);
            if (modelState)
            {
                User user = dataService.FindUser(u => u.Login == userName);
                dataService.AddOrder(new Order { OrderName = orderName, User = user });
                
                MessageBox.Show("Заказ успешно добавлен!");
            }
        }

        public void LogOut(Form form)
        {
            signInService.LogOut(form);
        }

    }
}
