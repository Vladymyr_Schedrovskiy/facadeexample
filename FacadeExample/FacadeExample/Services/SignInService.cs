﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacadeExample.Forms;
using FacadeExample.Models;

namespace FacadeExample.Services
{
    class SignInService
    {
        public void SignIn(User user, Form currentForm)
        {
            OrderForm orderForm = new OrderForm(user.Login);
            currentForm.Hide();
            orderForm.ShowDialog();
            currentForm.Dispose();
        }

        public void LogOut(Form currentForm)
        {
             LoginForm loginForm = new LoginForm();
            currentForm.Hide();
            loginForm.ShowDialog();
            currentForm.Close();
        }
    }
}
