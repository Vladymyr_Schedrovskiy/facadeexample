﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacadeExample.Models;

namespace FacadeExample.Services
{
    class DataBaseService : IDataService
    {
        private readonly ApplicationContext context;

        public DataBaseService()
        {
            context = new ApplicationContext();
        }

        public void AddOrder(Order order)
        {
            order.Id = Guid.NewGuid().ToString();
            context.Orders.Add(order);
            context.SaveChanges();
        }

        public void AddUser(User user)
        {
            user.Id = Guid.NewGuid().ToString();
            context.Users.Add(user);
            context.SaveChanges();
        }

        public void DeleteOrder(Order order)
        {
            throw new NotImplementedException();
        }

        public void DeleteUser(User user)
        {
            throw new NotImplementedException();
        }

        public Order FindOrder(Func<Order, bool> predicate)
        {
            return context.Orders.FirstOrDefault(predicate);
        }

        public User FindUser(Func<User, bool> predicate)
        {
            return context.Users.FirstOrDefault(predicate);
        }

        public List<Order> GetOrders(Func<Order, bool> predicate)
        {
            return context.Orders.Where(predicate).ToList();
        }

        public List<User> GetUsers(Func<User, bool> predicate)
        {
            return context.Users.Where(predicate).ToList();
        }
    }
}
