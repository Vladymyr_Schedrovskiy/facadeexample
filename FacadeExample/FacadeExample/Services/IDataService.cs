﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FacadeExample.Models;

namespace FacadeExample.Services
{
    public interface IDataService
    {
        User FindUser(Func<User, bool> predicate);
        Order FindOrder(Func<Order, bool> predicate);
        List<User> GetUsers(Func<User, bool> predicate);
        List<Order> GetOrders(Func<Order, bool> predicate);
        void AddUser(User user);
        void AddOrder(Order order);
        void DeleteUser(User user);
        void DeleteOrder(Order order);
    }
}