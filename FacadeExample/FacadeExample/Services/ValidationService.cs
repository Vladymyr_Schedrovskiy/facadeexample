﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacadeExample.Forms;
using FacadeExample.Models;

namespace FacadeExample.Services
{
    class ValidationService
    {
        private readonly IDataService dataService;

        public ValidationService(IDataService dataService)
        {
            this.dataService = dataService;
        }

        public bool ValidateRegisterForm(string login, string password, string confirmPassword, RegisterForm form)
        {
            bool modelState = true;
            if (string.IsNullOrEmpty(login))
            {
                form.login_validation.Text = "*Введите логин!";
                modelState = false;
            }
            else if(dataService.FindUser(u=>u.Login == login) != null)
            {
                form.login_validation.Text = "*Пользователь с таким именем уже существует!";
                modelState = false;
            }

            if (string.IsNullOrEmpty(password))
            {
                form.password_validation.Text = "*Введите пароль!";
                modelState = false;
            }

            if (password != confirmPassword)
            {
                form.confirm_validation.Text = "*Пароли не совпадают!";
                modelState = false;
            }
            return modelState;
        }

        public bool ValidateLoginForm(string login, string password, LoginForm form)
        {
            bool modelState = true;
            if (string.IsNullOrEmpty(login))
            {
                form.login_validation.Text = "*Введите логин!";
                modelState = false;
            }
            if (string.IsNullOrEmpty(password))
            {
                form.password_validation.Text = "*Введите пароль!";
                modelState = false;
            }
            if (!string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(password))
            {
                User user = dataService.FindUser(u => u.Login == login);
                if (user == null || user.Password != password)
                {
                    form.login_validation.Text = "*Неверный логин или пароль!";
                    modelState = false;
                }
            }
            return modelState;
        }

        public bool ValidateOrderForm(string orderName, OrderForm form)
        {
            bool modelState = true;
            if (string.IsNullOrEmpty(orderName))
            {
                modelState = false;
                form.orderNameValidation.Text = "*Укажите название заказа!";

            }
            return modelState;
        }
        
    }
}
