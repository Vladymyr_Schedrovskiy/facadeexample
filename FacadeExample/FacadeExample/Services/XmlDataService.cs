﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml;
using FacadeExample.Models;

namespace FacadeExample.Services
{
    public class XmlDataService : IDataService
    {
        private const string XmlFilePath = "../../Data.xml";

        public XmlDataService()
        {
            XmlDocument document = new XmlDocument();
            document.Load(XmlFilePath);
            var xRoot = document.DocumentElement;
            XmlNode usersXml = FindNodeByName("Users", xRoot);
            XmlNode ordersXml = FindNodeByName("Orders", xRoot);
            if (usersXml == null)
            {
                XmlDocumentFragment newUsersXml = document.CreateDocumentFragment();
                newUsersXml.InnerXml = "<Users></Users>";
                xRoot.AppendChild(newUsersXml);
                document.Save(XmlFilePath);
            }
            if (ordersXml == null)
            {
                XmlDocumentFragment newOrdersXml = document.CreateDocumentFragment();
                newOrdersXml.InnerXml = "<Orders></Orders>";
                xRoot.AppendChild(newOrdersXml);
                document.Save(XmlFilePath);
            }
          
        }


        public void AddOrder(Order order)
        {
            if (order.User != null && string.IsNullOrEmpty(order.UserId))
            {
                order.UserId = order.User.Id;
            }
            else
            {
                return;
            }

            XmlDocument document = new XmlDocument();
            document.Load(XmlFilePath);
            var xRoot = document.DocumentElement;
            XmlNode ordersXml = FindNodeByName("Orders", xRoot);
            order.Id = Guid.NewGuid().ToString();
            XmlDocumentFragment orderXml = document.CreateDocumentFragment();
            orderXml.InnerXml = string.Format("<order id=\"{0}\"><UserId>{1}</UserId><OrderName>{2}</OrderName></order>", order.Id, order.UserId, order.OrderName);
            ordersXml.AppendChild(orderXml);
            document.Save(XmlFilePath);

        }

        public void AddUser(User user)
        {
            XmlDocument document = new XmlDocument();
            document.Load(XmlFilePath);
            var xRoot = document.DocumentElement;
            XmlNode users = FindNodeByName("Users", xRoot);
            user.Id =Guid.NewGuid().ToString();
            XmlDocumentFragment userXml = document.CreateDocumentFragment();
            userXml.InnerXml = string.Format("<user id=\"{0}\"><login>{1}</login><password>{2}</password></user>",
                user.Id, user.Login, user.Password);
             users.AppendChild(userXml);
            document.Save(XmlFilePath);

            
        }

       



        public void DeleteOrder(Order order)
        {
            throw new NotImplementedException();
        }

        public void DeleteUser(User user)
        {
            throw new NotImplementedException();
        }

        public Order FindOrder(Func<Order, bool> predicate)
        {
            XmlDocument document = new XmlDocument();
            document.Load(XmlFilePath);
            var xRoot = document.DocumentElement;
            XmlNode ordersXml = FindNodeByName("Orders", xRoot);
            Order order = null;
            foreach (XmlNode xmlNode in ordersXml)
            {
                order = ParseOrder(xmlNode);
                if (predicate(order))
                {
                    return order;
                }
            }
            return null;
        }

        public User FindUser(Func<User, bool> predicate)
        {
            XmlDocument document = new XmlDocument();
            document.Load(XmlFilePath);
            var xRoot = document.DocumentElement;
            XmlNode usersXml = FindNodeByName("Users", xRoot);

            User user = null;
            foreach (XmlNode xmlNode in usersXml)
            {
                 user = ParseUser(xmlNode);
                if (predicate(user))
                {
                    return user;
                }
                
            }
           
            return null;
        }

        public List<Order> GetOrders(Func<Order, bool> predicate)
        {
            XmlDocument document = new XmlDocument();
            document.Load(XmlFilePath);
            var xRoot = document.DocumentElement;
            XmlNode ordersXml = FindNodeByName("Orders", xRoot);
           List<Order> orders = new List<Order>();
            foreach (XmlNode xmlNode in ordersXml)
            {
               Order order = ParseOrder(xmlNode);
                if (predicate(order))
                {
                    orders.Add(order);
                }
            }
            return orders;
        }

        public List<User> GetUsers(Func<User, bool> predicate)
        {
            XmlDocument document = new XmlDocument();
            document.Load(XmlFilePath);
            var xRoot = document.DocumentElement;
            XmlNode usersXml = FindNodeByName("Users", xRoot);

            List<User> users = new List<User>();
            foreach (XmlNode xmlNode in usersXml)
            {
                 User user = ParseUser(xmlNode);
                if (predicate(user))
                {
                    users.Add(user);
                }

            }
            return users;
        }

        private User ParseUser(XmlNode userNode)
        {
            User user = null;
            if (userNode.Name == "user")
            {
                user = new User();
                foreach (XmlAttribute attribute in userNode.Attributes)
                {
                    if (attribute.Name == "id")
                    {
                        user.Id = attribute.Value;
                    }
                    
                }
                foreach (XmlNode childNode in userNode.ChildNodes)
                {
                    if (childNode.Name == "login")
                    {
                        user.Login = childNode.InnerText;
                    }
                    else if (childNode.Name == "password")
                    {
                        user.Password = childNode.InnerText;
                    }
                }
            }
            return user;
        }
        private XmlNode FindNodeByName(string nodeName, XmlElement xmlElement)
        {
            XmlNode findNode = null;
            foreach (XmlNode node in xmlElement)
            {
                if (node.Name == nodeName)
                {
                    findNode = node;
                    return findNode;
                }
            }
            return findNode;
        }
        private XmlNode FindNodeByNameAndId(string nodeName, string id, XmlElement xmlElement)
        {
            XmlNode findNode = null;
            foreach (XmlNode node in xmlElement)
            {
                if (node.Name == nodeName)
                {
                    foreach (XmlAttribute nodeAttribute in node.Attributes)
                    {
                        if (nodeAttribute.Name == "id" && nodeAttribute.Value == id)
                        {
                            findNode = node;
                            return findNode;
                        }
                    }


                }
            }
            return findNode;
        }

        private Order ParseOrder(XmlNode orderNode)
        {
            Order order = null;
            if (orderNode.Name == "order")
            {
                order = new Order();
                foreach (XmlAttribute attribute in orderNode.Attributes)
                {
                    if (attribute.Name == "id")
                    {
                        order.Id = attribute.Value;
                    }

                }
                foreach (XmlNode childNode in orderNode.ChildNodes)
                {
                    if (childNode.Name == "UserId")
                    {
                        order.UserId = childNode.InnerText;
                    }
                    else if (childNode.Name == "OrderName")
                    {
                        order.OrderName = childNode.InnerText;
                    }
                }
            }
            return order;
        }
    }
}