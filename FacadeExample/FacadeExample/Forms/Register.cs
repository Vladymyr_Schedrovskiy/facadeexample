﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacadeExample.Facades;
using FacadeExample.Models;
using FacadeExample.Services;

namespace FacadeExample.Forms
{
    public partial class RegisterForm : Form
    {
        private readonly UserFacade userFacade;
        public RegisterForm()
        {
            userFacade = new UserFacade();
            InitializeComponent();
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            ClearValidationLabels();
            string login = login_input.Text.Trim();
            string password = password_input.Text.Trim();
            string passwordConfirm = confirm_input.Text.Trim();
           userFacade.Register(login, password, passwordConfirm, this);
           
            
        }

        private void SignInLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LoginForm form = new LoginForm();
            this.Hide();
            form.Show();
            this.Dispose();
        }

        private void ClearValidationLabels()
        {
            login_validation.Text = String.Empty;
            password_validation.Text = String.Empty;
            confirm_label.Text = String.Empty;
        }

      
        private void password_input_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
