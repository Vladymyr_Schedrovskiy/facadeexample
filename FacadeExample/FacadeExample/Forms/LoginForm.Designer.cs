﻿namespace FacadeExample
{
    partial class LoginForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.login_input = new System.Windows.Forms.TextBox();
            this.password_input = new System.Windows.Forms.TextBox();
            this.login_label = new System.Windows.Forms.Label();
            this.password_label = new System.Windows.Forms.Label();
            this.sign_in_button = new System.Windows.Forms.Button();
            this.register_link = new System.Windows.Forms.LinkLabel();
            this.login_validation = new System.Windows.Forms.Label();
            this.password_validation = new System.Windows.Forms.Label();
            this.validation_all = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // login_input
            // 
            this.login_input.Location = new System.Drawing.Point(27, 49);
            this.login_input.Name = "login_input";
            this.login_input.Size = new System.Drawing.Size(122, 20);
            this.login_input.TabIndex = 0;
            // 
            // password_input
            // 
            this.password_input.Location = new System.Drawing.Point(27, 119);
            this.password_input.Name = "password_input";
            this.password_input.Size = new System.Drawing.Size(122, 20);
            this.password_input.TabIndex = 1;
            // 
            // login_label
            // 
            this.login_label.AutoSize = true;
            this.login_label.Location = new System.Drawing.Point(24, 33);
            this.login_label.Name = "login_label";
            this.login_label.Size = new System.Drawing.Size(33, 13);
            this.login_label.TabIndex = 2;
            this.login_label.Text = "Login";
            // 
            // password_label
            // 
            this.password_label.AutoSize = true;
            this.password_label.Location = new System.Drawing.Point(24, 103);
            this.password_label.Name = "password_label";
            this.password_label.Size = new System.Drawing.Size(53, 13);
            this.password_label.TabIndex = 3;
            this.password_label.Text = "Password";
            // 
            // sign_in_button
            // 
            this.sign_in_button.Location = new System.Drawing.Point(27, 172);
            this.sign_in_button.Name = "sign_in_button";
            this.sign_in_button.Size = new System.Drawing.Size(119, 24);
            this.sign_in_button.TabIndex = 4;
            this.sign_in_button.Text = "Sign In";
            this.sign_in_button.UseVisualStyleBackColor = true;
            this.sign_in_button.Click += new System.EventHandler(this.sign_in_button_Click);
            // 
            // register_link
            // 
            this.register_link.AutoSize = true;
            this.register_link.Location = new System.Drawing.Point(31, 230);
            this.register_link.Name = "register_link";
            this.register_link.Size = new System.Drawing.Size(46, 13);
            this.register_link.TabIndex = 5;
            this.register_link.TabStop = true;
            this.register_link.Text = "Register";
            this.register_link.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.register_link_LinkClicked);
            // 
            // login_validation
            // 
            this.login_validation.AutoSize = true;
            this.login_validation.ForeColor = System.Drawing.Color.Red;
            this.login_validation.Location = new System.Drawing.Point(31, 72);
            this.login_validation.Name = "login_validation";
            this.login_validation.Size = new System.Drawing.Size(0, 13);
            this.login_validation.TabIndex = 6;
            // 
            // password_validation
            // 
            this.password_validation.AutoSize = true;
            this.password_validation.ForeColor = System.Drawing.Color.Red;
            this.password_validation.Location = new System.Drawing.Point(31, 142);
            this.password_validation.Name = "password_validation";
            this.password_validation.Size = new System.Drawing.Size(0, 13);
            this.password_validation.TabIndex = 7;
            // 
            // validation_all
            // 
            this.validation_all.AutoSize = true;
            this.validation_all.ForeColor = System.Drawing.Color.Red;
            this.validation_all.Location = new System.Drawing.Point(127, 9);
            this.validation_all.Name = "validation_all";
            this.validation_all.Size = new System.Drawing.Size(0, 13);
            this.validation_all.TabIndex = 9;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.validation_all);
            this.Controls.Add(this.password_validation);
            this.Controls.Add(this.login_validation);
            this.Controls.Add(this.register_link);
            this.Controls.Add(this.sign_in_button);
            this.Controls.Add(this.password_label);
            this.Controls.Add(this.login_label);
            this.Controls.Add(this.password_input);
            this.Controls.Add(this.login_input);
            this.Name = "LoginForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox login_input;
        public System.Windows.Forms.TextBox password_input;
        private System.Windows.Forms.Label login_label;
        private System.Windows.Forms.Label password_label;
        private System.Windows.Forms.Button sign_in_button;
        private System.Windows.Forms.LinkLabel register_link;
        public System.Windows.Forms.Label login_validation;
        public System.Windows.Forms.Label password_validation;
        public System.Windows.Forms.Label validation_all;
    }
}

