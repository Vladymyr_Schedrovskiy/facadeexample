﻿namespace FacadeExample.Forms
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.validation_all = new System.Windows.Forms.Label();
            this.password_validation = new System.Windows.Forms.Label();
            this.login_validation = new System.Windows.Forms.Label();
            this.RegisterButton = new System.Windows.Forms.Button();
            this.password_label = new System.Windows.Forms.Label();
            this.login_label = new System.Windows.Forms.Label();
            this.password_input = new System.Windows.Forms.TextBox();
            this.login_input = new System.Windows.Forms.TextBox();
            this.confirm_input = new System.Windows.Forms.TextBox();
            this.confirm_label = new System.Windows.Forms.Label();
            this.confirm_validation = new System.Windows.Forms.Label();
            this.SignInLabel = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // validation_all
            // 
            this.validation_all.AutoSize = true;
            this.validation_all.ForeColor = System.Drawing.Color.Red;
            this.validation_all.Location = new System.Drawing.Point(127, 0);
            this.validation_all.Name = "validation_all";
            this.validation_all.Size = new System.Drawing.Size(0, 13);
            this.validation_all.TabIndex = 18;
            // 
            // password_validation
            // 
            this.password_validation.AutoSize = true;
            this.password_validation.ForeColor = System.Drawing.Color.Red;
            this.password_validation.Location = new System.Drawing.Point(31, 127);
            this.password_validation.Name = "password_validation";
            this.password_validation.Size = new System.Drawing.Size(0, 13);
            this.password_validation.TabIndex = 17;
            // 
            // login_validation
            // 
            this.login_validation.AutoSize = true;
            this.login_validation.ForeColor = System.Drawing.Color.Red;
            this.login_validation.Location = new System.Drawing.Point(31, 63);
            this.login_validation.Name = "login_validation";
            this.login_validation.Size = new System.Drawing.Size(0, 13);
            this.login_validation.TabIndex = 16;
            // 
            // RegisterButton
            // 
            this.RegisterButton.Location = new System.Drawing.Point(27, 225);
            this.RegisterButton.Name = "RegisterButton";
            this.RegisterButton.Size = new System.Drawing.Size(119, 24);
            this.RegisterButton.TabIndex = 14;
            this.RegisterButton.Text = "Register";
            this.RegisterButton.UseVisualStyleBackColor = true;
            this.RegisterButton.Click += new System.EventHandler(this.RegisterButton_Click);
            // 
            // password_label
            // 
            this.password_label.AutoSize = true;
            this.password_label.Location = new System.Drawing.Point(24, 88);
            this.password_label.Name = "password_label";
            this.password_label.Size = new System.Drawing.Size(53, 13);
            this.password_label.TabIndex = 13;
            this.password_label.Text = "Password";
            // 
            // login_label
            // 
            this.login_label.AutoSize = true;
            this.login_label.Location = new System.Drawing.Point(24, 24);
            this.login_label.Name = "login_label";
            this.login_label.Size = new System.Drawing.Size(33, 13);
            this.login_label.TabIndex = 12;
            this.login_label.Text = "Login";
            // 
            // password_input
            // 
            this.password_input.Location = new System.Drawing.Point(27, 104);
            this.password_input.Name = "password_input";
            this.password_input.Size = new System.Drawing.Size(122, 20);
            this.password_input.TabIndex = 11;
            this.password_input.TextChanged += new System.EventHandler(this.password_input_TextChanged);
            // 
            // login_input
            // 
            this.login_input.Location = new System.Drawing.Point(27, 40);
            this.login_input.Name = "login_input";
            this.login_input.Size = new System.Drawing.Size(122, 20);
            this.login_input.TabIndex = 10;
            // 
            // confirm_input
            // 
            this.confirm_input.Location = new System.Drawing.Point(27, 168);
            this.confirm_input.Name = "confirm_input";
            this.confirm_input.Size = new System.Drawing.Size(122, 20);
            this.confirm_input.TabIndex = 19;
            // 
            // confirm_label
            // 
            this.confirm_label.AutoSize = true;
            this.confirm_label.Location = new System.Drawing.Point(24, 152);
            this.confirm_label.Name = "confirm_label";
            this.confirm_label.Size = new System.Drawing.Size(91, 13);
            this.confirm_label.TabIndex = 20;
            this.confirm_label.Text = "Confirm Password";
            // 
            // confirm_validation
            // 
            this.confirm_validation.AutoSize = true;
            this.confirm_validation.ForeColor = System.Drawing.Color.Red;
            this.confirm_validation.Location = new System.Drawing.Point(24, 191);
            this.confirm_validation.Name = "confirm_validation";
            this.confirm_validation.Size = new System.Drawing.Size(0, 13);
            this.confirm_validation.TabIndex = 21;
            // 
            // SignInLabel
            // 
            this.SignInLabel.AutoSize = true;
            this.SignInLabel.Location = new System.Drawing.Point(205, 236);
            this.SignInLabel.Name = "SignInLabel";
            this.SignInLabel.Size = new System.Drawing.Size(40, 13);
            this.SignInLabel.TabIndex = 22;
            this.SignInLabel.TabStop = true;
            this.SignInLabel.Text = "Sign In";
            this.SignInLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.SignInLabel_LinkClicked);
            // 
            // RegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.SignInLabel);
            this.Controls.Add(this.confirm_validation);
            this.Controls.Add(this.confirm_label);
            this.Controls.Add(this.confirm_input);
            this.Controls.Add(this.validation_all);
            this.Controls.Add(this.password_validation);
            this.Controls.Add(this.login_validation);
            this.Controls.Add(this.RegisterButton);
            this.Controls.Add(this.password_label);
            this.Controls.Add(this.login_label);
            this.Controls.Add(this.password_input);
            this.Controls.Add(this.login_input);
            this.Name = "RegisterForm";
            this.Text = "RegisterForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label validation_all;
        public System.Windows.Forms.Label password_validation;
        public System.Windows.Forms.Label login_validation;
        private System.Windows.Forms.Button RegisterButton;
        private System.Windows.Forms.Label password_label;
        private System.Windows.Forms.Label login_label;
        public System.Windows.Forms.TextBox password_input;
        public System.Windows.Forms.TextBox login_input;
        private System.Windows.Forms.TextBox confirm_input;
        private System.Windows.Forms.Label confirm_label;
        public System.Windows.Forms.Label confirm_validation;
        private System.Windows.Forms.LinkLabel SignInLabel;
    }
}