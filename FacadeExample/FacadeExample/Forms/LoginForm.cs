﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacadeExample.Facades;
using FacadeExample.Forms;
using FacadeExample.Models;
using FacadeExample.Services;
using Microsoft.Win32;

namespace FacadeExample
{
    public partial class LoginForm : Form
    {
        private readonly UserFacade userFacade;
        public LoginForm()
        {
           userFacade = new UserFacade();
            InitializeComponent();
         }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void sign_in_button_Click(object sender, EventArgs e)
        {
           ClearValidationLabels(); 
            string login = login_input.Text.Trim();
            string password = password_input.Text.Trim();
           userFacade.SignIn(login, password, this);

        }

        private void register_link_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RegisterForm registerFormForm = new RegisterForm();
            this.Hide();
            registerFormForm.ShowDialog();
           this.Dispose();
        }

        private void ClearValidationLabels()
        {
            login_validation.Text = String.Empty;
            password_validation.Text = String.Empty;
        }
    }
}
