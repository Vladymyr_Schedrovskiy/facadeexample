﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacadeExample.Facades;
using FacadeExample.Models;
using FacadeExample.Services;

namespace FacadeExample.Forms
{
    public partial class OrderForm : Form
    {
        public string UserName { get; set; }
        private readonly UserFacade userFacade;
        public OrderForm(string userName)
        {
            userFacade = new UserFacade();
            InitializeComponent();
            UserName = userName;
        }

        private void orderAddButton_Click(object sender, EventArgs e)
        {
            orderNameValidation.Text = string.Empty;

            string orderName = orderNameInput.Text.Trim();
           userFacade.AddOrder(orderName, UserName, this);
            orderNameInput.Text = string.Empty;
            


        }

        private void logOutLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            userFacade.LogOut(this);
        }
    }
}
