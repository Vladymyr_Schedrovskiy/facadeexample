﻿namespace FacadeExample.Forms
{
    partial class OrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.orderNameInput = new System.Windows.Forms.TextBox();
            this.order_name_label = new System.Windows.Forms.Label();
            this.orderAddButton = new System.Windows.Forms.Button();
            this.orderNameValidation = new System.Windows.Forms.Label();
            this.logOutLink = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(62, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "Сделать заказ";
            // 
            // orderNameInput
            // 
            this.orderNameInput.Location = new System.Drawing.Point(67, 110);
            this.orderNameInput.Name = "orderNameInput";
            this.orderNameInput.Size = new System.Drawing.Size(126, 20);
            this.orderNameInput.TabIndex = 1;
            // 
            // order_name_label
            // 
            this.order_name_label.AutoSize = true;
            this.order_name_label.Location = new System.Drawing.Point(81, 94);
            this.order_name_label.Name = "order_name_label";
            this.order_name_label.Size = new System.Drawing.Size(96, 13);
            this.order_name_label.TabIndex = 2;
            this.order_name_label.Text = "Название заказа";
            // 
            // orderAddButton
            // 
            this.orderAddButton.Location = new System.Drawing.Point(83, 157);
            this.orderAddButton.Name = "orderAddButton";
            this.orderAddButton.Size = new System.Drawing.Size(94, 21);
            this.orderAddButton.TabIndex = 3;
            this.orderAddButton.Text = "Добавить";
            this.orderAddButton.UseVisualStyleBackColor = true;
            this.orderAddButton.Click += new System.EventHandler(this.orderAddButton_Click);
            // 
            // orderNameValidation
            // 
            this.orderNameValidation.AutoSize = true;
            this.orderNameValidation.ForeColor = System.Drawing.Color.Red;
            this.orderNameValidation.Location = new System.Drawing.Point(64, 133);
            this.orderNameValidation.Name = "orderNameValidation";
            this.orderNameValidation.Size = new System.Drawing.Size(0, 13);
            this.orderNameValidation.TabIndex = 4;
            // 
            // logOutLink
            // 
            this.logOutLink.AutoSize = true;
            this.logOutLink.Location = new System.Drawing.Point(220, 9);
            this.logOutLink.Name = "logOutLink";
            this.logOutLink.Size = new System.Drawing.Size(45, 13);
            this.logOutLink.TabIndex = 5;
            this.logOutLink.TabStop = true;
            this.logOutLink.Text = "Log Out";
            this.logOutLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.logOutLink_LinkClicked);
            // 
            // OrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.logOutLink);
            this.Controls.Add(this.orderNameValidation);
            this.Controls.Add(this.orderAddButton);
            this.Controls.Add(this.order_name_label);
            this.Controls.Add(this.orderNameInput);
            this.Controls.Add(this.label1);
            this.Name = "OrderForm";
            this.Text = "OrderForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox orderNameInput;
        private System.Windows.Forms.Label order_name_label;
        private System.Windows.Forms.Button orderAddButton;
        public System.Windows.Forms.Label orderNameValidation;
        private System.Windows.Forms.LinkLabel logOutLink;
    }
}