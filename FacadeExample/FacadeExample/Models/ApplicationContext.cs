﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacadeExample.Models
{
    class ApplicationContext : DbContext
    {
        public ApplicationContext()
            : base("DBConnection")
        { }

        public DbSet<User> Users { get; set; }  
        public DbSet<Order> Orders { get; set; }
    }
}
